package Negocio;


/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetras
{
    // instance variables - replace the example below with your own
    private char sopa[][];
int nFila=0;
    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras()
    {
        
    }

    
    
    
    
    
    public SopaDeLetras(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }
        
     //Crear la matriz con las correspondientes filas:
     
     String palabras2[]=palabras.split(",");
     this.sopa=new char[palabras2.length][];
     int i=0;
     for(String palabraX:palabras2)
     {
         //Creando las columnas de la fila i
         this.sopa[i]=new char[palabraX.length()];
         pasar(palabraX,this.sopa[i]);
         i++;
        
     }
     
     
    }
    
    private void pasar (String palabra, char fila[])
    {
    
        for(int j=0;j<palabra.length();j++)
        {
            fila[j]=palabra.charAt(j);
        }
    }
    
    
    public String toString()
    {
    String msg="";
    for(int i=0;i<this.sopa.length;i++)
    {
        for (int j=0;j<this.sopa[i].length;j++)
        {
            msg+=this.sopa[i][j]+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    public String toString2()
    {
    String msg="";
    for(char filas[]:this.sopa)
    {
        for (char dato :filas)
        {
            msg+=dato+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    public boolean esCuadrada()
    {    
        int fila=sopa.length;
        for(int i=0;i<sopa.length;i++){
            for(int j=0;j<sopa[i].length;j++){
                if(fila!=sopa[i].length)
                    return false;
            }
        }
        return true;
    }
    
    public boolean esDispersa3(){

        int columna=sopa[0].length;
        //si el array solo tiene una fila no sera dispersa
        if(this.sopa.length==1){        
            return false;
        }

        else{
            for(int i=0;i<sopa.length;i++){
                for(int j=0;j<sopa[i].length;j++){
                    //compruebo si cada columna es diferente a la primera columna
                    if(sopa[i].length!=columna)
                        return true;
                }
            } 
            return false;
        }

    }

    public char []getDiagonalInferior() throws Exception
    {
        char letras[]=new char[sopa.length];
        if(!esCuadrada()){
            throw new Exception("No es cuadrada");
        }
        else{
            for(int i=0;i<sopa.length;i++){
                for(int j=0;j<sopa[i].length;j++){
                    if(i==j){
                        letras[i]=sopa[i][j];
                    }
                }
            }      
        }
        //si y solo si es cuadrada , si no, lanza excepcion
        return letras;
    }
    
public void buscarPalabra(String palabra) throws Exception{ 

        //verifico si la matriz no es dispersa o si la palabra ingresada es mas grande que las filas
        if(esDispersa3() || palabra.length()>sopa.length || palabra.length()==1)
            throw new Exception ("no es posible realizar la busqueda" );
        else{

            for(int i=0;i<sopa.length;i++){
                for(int j=0;j<sopa[i].length;j++){

                }
                nFila=i;
                buscarPalabraFila(palabra.toCharArray(),sopa[i]);

            }
        }

    }

    public boolean palabraAtras(char [] palabra,char [] vector ){
        
        int j=0;
        int k=0;
        int k1=0;
        
        char vectReves[]=new char[palabra.length];
        for(int i=palabra.length-1;i>=0;i--,j++){
            vectReves[j]=palabra[i];
        }
        
        while(k<vector.length && k1!=palabra.length ){
        
            if(vector[k]==vectReves[k1] ){
                k1++;
                if(k1==palabra.length){
                    System.out.println("palabra de derecha a izq ");
                    return true;
                }

            }
            else k++;
        }
        return false;
    }

    public void buscarPalabraFila(char[] palabra,char[] vector){
        //recorrido de la palabra buscada
        int j=0;
        int i=0;
        
        if(!palabraAtras(palabra,vector)){
        while(i<vector.length && j!=palabra.length ){
            if(vector[i]==palabra[j] ){
                j++;
                if(j==palabra.length){
                    System.out.println ("fila"+nFila);
                    System.out.println ("columna"+(i-(palabra.length-1)));//obtengo la columna donde halle la palabra
                    System.out.println("palabra de izq a derecha");
                }

            }
            else i++;
        
                  }
    }
    } 

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopa; 
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
}
